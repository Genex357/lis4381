> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Assignment 1 Requirements:

1. Distributed Control Setup
2. Development Instillations
3. Questions

#### README.md file should include the following items:

* Screenshot of Ammps running
* Screenshot of Java "Hello World"
* Screenshot of Android Studio's "My first App"
* Git commands
* Bitbucket repo links

> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one.
2. git status: Show the working tree status.
3. git add: Add file contents to the index.
4. git commit: Saves the changes to the repository.
5. git push: Updates remote refs along with associated objects.
6. git pull: Fetch from / integrate with another repository / a local branch.
7. git config: Configures the author's name and email to your commits.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4381/src/master/bitbucketstationlocations)

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
